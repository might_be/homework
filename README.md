# 2018年度 目標管理 課題

## 概要

2017年度に製作した、Raspberry Pi を利用したIoTデバイスの機能アップデート。

-----------------------
## 開発環境

実機 : Raspberry Pi 3  
OS : Raspbian

-----------------------
## 2018年度（Ver 2.0）

### ホームセキュリティ

- 動画撮影に対応

#### 人感センサ

- 赤外線センサ＋赤外線LEDで検知
- センサの検知でカメラ撮影→LINE通知
	- 赤外線カメラにアップグレード、夜間撮影に対応
	- 動画撮影も可能
- 設定変更
	- LINE BOTを拡張
		- デイリータイマー設定
		- 手動監視ON/OFF
		- 静止画撮影/動画撮影の切り替え

### UI改善

- リッチメニューに対応

-----------------------
## 2017年度（Ver 1.0）

### ホームセキュリティ

#### LINE BOT

- 外部からのUIとなる
- LINE ⇔ Heroku(Node.js) ⇔ MQTT Brokerサーバ ⇔ Raspberry Pi

#### 外出先から監視カメラ撮影

- ~~Python~~ Node.js
- LINEから操作
- 撮影した画像をdropboxにアップロード、LINEに返信

-----------------------
## 将来対応

### Pythonへのリプレイス

Raspberry Pi に組み込むコードを Node.js → Python に。  
（学習のため。一般的にPythonよりNode.jsの方が動作は高速）  
Heroku上はHTTPサーバ用途に最適なNode.jsで継続。

### スマートホーム

- 照明操作
- エアコン操作

### ログ機能・その他

- Push通知のブロードキャスト（1対N）
- エラーログ
- アクセスログ
- キャッシュデータ（画像・動画）のクリーニング

-----------------------
## システム構成図

[拡大](https://bytebucket.org/might_be/homework/raw/a685a9e2104b08725c3f2497497e1fc427bc23e8/Component.png?token=ed0369479a6e85b48cddc5e1c24c2d0016449742)
![構成図](Component.png)

-----------------------
## 使用ツール・サービス

### ツール

- [Node.js](https://nodejs.org/ja/)  
サーバサイドjavascript。  
Heroku/Raspberry Piで使用。

- Heroku CLI  
コマンドラインからHerokuを操作する。

- FFmpeg  
動画と音声を記録・変換・再生するLinuxパッケージ。  
静止画や動画からサムネイル用画像を作成するために使用。

- Git  
HerokuやBitBucketへのコミットに使用。

### サービス

- [LINE Messaging API](https://developers.line.me/ja/)  
LINE BOTの作成サービス。  
外部からのUIとして使用。

- [Heroku](https://www.heroku.com/)  
クラウドアプリ構築サービス。  
LINE messaging APIのwebhookに使用。  
	- 認定認証局で発行されたSSL証明書が必要であるため。

- [Cloud MQTT](https://www.cloudmqtt.com/)  
自作のMQTT Brokerサーバ構築環境を提供する。  
HerokuとRaspberry Pi間の通信に使用。  
Raspberry Piを外部ネットワークに公開せずに外部からの通信を受けられる。

- [Dropbox](https://www.dropbox.com/ja/)  
クラウドストレージサービス。  
ここにキャプチャ画像/動画をアップロードする。
	- LINE messaging APIに画像/動画を送信するためには、サーバ上の画像のURLを指定する必要があるため。

