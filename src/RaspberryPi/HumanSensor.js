//==============================================================================
// require
//==============================================================================
var gpio = require('rpi-gpio');
var gpiop = gpio.promise;
var async = require('async');
var __config = require('./Config.js');
var __mqtt = require('./Mqtt.js');

//==============================================================================
// 定義
//==============================================================================
const GPIO_PIN = 18;			// GPIOピン番号

//==============================================================================
// export
//==============================================================================
module.exports = {
	start
};

//==============================================================================
// 関数
//==============================================================================
function start() {
	gpio.setMode(gpio.MODE_BCM);
	gpiop.setup(GPIO_PIN, gpio.DIR_IN)
		.then(function () {
			// GPIO定周期チェック開始
			setTimeout(__checkSensor, __config.getHSCyclicMsec());
			// タイマーON/OFF通知チェック開始
			setInterval(__checkTimer, 60000);	// 1分周期固定
		})
		.catch(function (error) {
			console.log('[HS]ERROR: ' + error.toString());
		});
}

function __checkSensor() {
	// 手動監視ON、またはデイリータイマー時刻範囲内であれば、実行
	if ((__config.getHSManual() === true) || (__config.isNowTimerZone() === true)) {
		async.waterfall([	// 順次処理
			function (callback) {
				// GPIO読み出し
				gpiop.read(GPIO_PIN)
					.then(function (isOn) {
						callback(null, isOn);
					})
					.catch(function (error) {
						callback('[HS]ERROR: ' + error.toString(), 'gpio read');
					});
			},
			function (isOn, callback) {
				// 通知処理
				if (isOn === true) {
					console.log('[HS]Human Detected.');
					__mqtt.sendCapture(__config.getHSFileType(), true);
					__mqtt.sendText('\u{100035}CAUTION\u{100035}\n侵入を検知しました', true);
				}
				callback(null, isOn);
			}
		], function (error, isOn) {
			if (error) {
				console.log(error);
			}
			console.log('[HS]isOn: ' + isOn);
			if (isOn === true) {
				// 検知した場合は再スキャン時間までディレイ
				setTimeout(__checkSensor, __config.getReScaningMsec());
			}
			else {
				// 検知しなかった場合は通常の定周期
				setTimeout(__checkSensor, __config.getHSCyclicMsec());
			}
		});
	}
	else {
		setTimeout(__checkSensor, __config.getHSCyclicMsec());
	}
}

function __checkTimer() {
	if (__config.checkTimerStart() === true) {
		__mqtt.sendText('\u{100077}INFO\u{100077}\nセキュリティON時刻です', true);
	}
	if (__config.checkTimerEnd() === true) {
		__mqtt.sendText('\u{100077}INFO\u{100077}\nセキュリティOFF時刻です', true);
	}
}
