//==============================================================================
// require
//==============================================================================
var mqtt = require('mqtt');
var __capture = require('./Capture.js');
var __config = require('./Config.js');

//==============================================================================
// 定義
//==============================================================================
const STATE_IDLE = 0;
const STATE_CONFIG = 1;
const STATE_SET_TIMERON = 2;
const STATE_SET_TIMEROFF = 3;
const STATE_FILETYPE = 4;

const HELP_MSG =
	'\
【ヘルプ】\n\
以下のキーワードで操作\n\
\n\
"カメラ/静止画/撮影"\n\
- 静止画撮影\n\n\
"ビデオ/動画"\n\
- 動画撮影(5sec)\n\n\
"セキュリティON"\n\
- 手動でセキュリティをON\n\n\
"セキュリティOFF"\n\
- 手動でセキュリティをOFF\n\
"設定"\n\
- 設定項目を表示\n\n\
';															// ヘルプメッセージ
var ERRMSG_CAPTURE = '[ERROR]\n撮影に失敗しました\u{100099}';	// エラーメッセージ（キャプチャ）

//==============================================================================
// 変数
//==============================================================================
var isConnected = false;	// 接続完了フラグ
var client;					// クライアント
var state = STATE_IDLE;

//==============================================================================
// export
//==============================================================================
module.exports = {
	start,
	sendText,
	sendCapture
};

//==============================================================================
// 関数
//==============================================================================
function start() {
	// Brokerへ接続
	client = mqtt.connect('mqtt://cyamuxhm:t8oJ7Gl_287U@m10.cloudmqtt.com:14459');

	// 接続完了イベントハンドラ
	client.on('connect', function () {
		console.log('[MQTT]connected.');
		// サブスクライブ開始
		client.subscribe('homework-pi/to-pi');
		isConnected = true;
	})

	// メッセージ受信イベントハンドラ
	client.on('message', function (topic, message) {
		// message is Buffer
		var str = message.toString();
		console.log('[MQTT]' + str);
		if (state === STATE_IDLE) {
			switch (str) {
				case 'カメラ':
				case '静止画':
				case '撮影':
					// 静止画撮影リクエスト
					__sendCapture('IMAGE', false, ERRMSG_CAPTURE);
					break;

				case 'ビデオ':
				case '動画':
					// 動画撮影リクエスト
					__sendCapture('VIDEO', false, ERRMSG_CAPTURE);
					break;

				case 'セキュリティON':
					__config.setHSManual(true);
					__sendText('セキュリティONに変更完了', false);
					break;

				case 'セキュリティOFF':
					__config.setHSManual(false);
					__sendText('セキュリティOFFに変更完了', false);
					break;

				case '設定':
					// 設定項目を表示
					__sendText(__config.CONFIG_MSG, false);
					state = STATE_CONFIG;
					break;

				case 'ヘルプ':
					// ヘルプメッセージを送信
					__sendText(HELP_MSG + "セキュリティ：" + __config.getHSManual() + "\n", false);
					break;

				default:
					// 不明
					// ヘルプメッセージを送信
					__sendText(HELP_MSG + "セキュリティ：" + __config.getHSManual() + "\n", false);
					break;
			}
		}
		else if (state == STATE_CONFIG) {
			switch (str) {
				case "タイマー開始":
					state = STATE_SET_TIMERON;
					__sendText(__config.TIMERON_MSG + "現在値：" + __config.getTimerOn() + "\n", false);
					break;

				case "タイマー終了":
					state = STATE_SET_TIMEROFF;
					__sendText(__config.TIMEROFF_MSG + "現在値：" + __config.getTimerOff() + "\n", false);
					break;

				case "通知タイプ":
					state = STATE_FILETYPE;
					__sendText(__config.FILETIPE_MSG + "現在値：" + __config.getHSFileType() + "\n", false);
					break;

				case "終了":
					// 設定メニューを終了
					state = STATE_IDLE;
					__sendText('設定メニュー終了', false);
					break;

				default:
					// 不明
					__sendText(__config.CONFIG_MSG, false);
					break;
			}
		}
		else if (state == STATE_SET_TIMERON) {
			if (__config.setTimerOn(str)) {
				__sendText(__config.COMPLETE_MSG, false);
			}
			else {
				__sendText(__config.ERROR_MSG, false);
			}
			state = STATE_IDLE;
		}
		else if (state == STATE_SET_TIMEROFF) {
			if (__config.setTimerOff(str)) {
				__sendText(__config.COMPLETE_MSG, false);
			}
			else {
				__sendText(__config.ERROR_MSG, false);
			}
			state = STATE_IDLE;
		}
		else if (state == STATE_FILETYPE) {
			switch (str) {
				case "画像":
					if (__config.setHSFileType('IMAGE') === true) {
						__sendText(__config.COMPLETE_MSG, false);
					}
					else {
						__sendText(__config.ERROR_MSG, false);
					}
					state = STATE_IDLE;
					break;

				case "動画":
					if (__config.setHSFileType('VIDEO') === true) {
						__sendText(__config.COMPLETE_MSG, false);
					}
					else {
						__sendText(__config.ERROR_MSG, false);
					}
					state = STATE_IDLE;
					break;

				case "設定終了":
					// 設定メニューを終了
					state = STATE_IDLE;
					__sendText('設定メニュー終了', false);
					break;

				default:
					// 不明
					__sendText(__config.FILETIPE_MSG + "現在値：" + __config.getHSFileType() + "\n", false);
					break;
			}
		}
		else {
			console.log('[MQTT]Invalid state.');
		}
	})
}

function sendText(message, isPush) {
	if (!isConnected) {
		return;
	}
	__sendText(message, isPush);
}

function sendCapture(operation, isPush) {
	if (!isConnected) {
		return;
	}
	__sendCapture(operation, isPush, null);
}

function __sendText(message, isPush) {
	var topic
	if (isPush === true) {
		console.log('[MQTT]push');
		topic = 'homework-pi/from-pi/push/text';
	}
	else {
		console.log('[MQTT]reply');
		topic = 'homework-pi/from-pi/reply/text';
	}
	client.publish(topic, message);
}

function __sendCapture(operation, isPush, errorMsg) {
	__capture.execute(operation)
		.then(function (value) {
			var jsonStr = JSON.stringify(value);
			var topic
			if (operation === 'IMAGE') {
				if (isPush === true) {
					console.log('[MQTT]push');
					topic = 'homework-pi/from-pi/push/image';
				}
				else {
					console.log('[MQTT]reply');
					topic = 'homework-pi/from-pi/reply/image';
				}
				client.publish(topic, jsonStr);
			}
			else if (operation === 'VIDEO') {
				if (isPush === true) {
					console.log('[MQTT]push');
					topic = 'homework-pi/from-pi/push/video';
				}
				else {
					console.log('[MQTT]reply');
					topic = 'homework-pi/from-pi/reply/video';
				}
				client.publish(topic, jsonStr);
			}
			else {
				console.log('[MQTT]Invalid operation.');
			}
		})
		.catch(function (error) {
			console.log('[MQTT]ERROR: ' + error.toString());
			if (errorMsg) {
				client.publish('homework-pi/from-pi/text', errorMsg);
			}
		});
}
