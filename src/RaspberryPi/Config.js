//==============================================================================
// require
//==============================================================================
const fs = require('fs');
var config = require('./config.json');

//==============================================================================
// 定義
//==============================================================================
const CONFIG_MSG =
	'\
【設定メニュー】\n\
以下のキーワードから選択\n\
\n\
"タイマー開始"\n\
- セキュリティ開始時刻\n\n\
"タイマー終了"\n\
- セキュリティ終了時刻\n\n\
"通知タイプ"\n\
- 静止画/動画から選択\n\n\
"終了"\n\
- 設定メニューを終了\n\n\
';															// 設定一覧メッセージ
const COMPLETE_MSG = '設定完了'
const ERROR_MSG = '設定失敗'
const TIMERON_MSG =
	'\
【タイマー開始】\n\
以下の形式で時間を入力(24h表記)\n\
\n\
hhmm\n\n\
タイマー機能OFFの場合は\n\
"OFF"\n\n\
';
const TIMEROFF_MSG =
	'\
【タイマー終了】\n\
以下の形式で時間を入力(24h表記)\n\
\n\
hhmm\n\n\
';
const FILETIPE_MSG =
	'\
【通知タイプ】\n\
以下のキーワードから選択\n\
\n\
”画像”\n\
"動画"\n\n\
"設定終了"\n\n\
';

//==============================================================================
// 変数
//==============================================================================

//==============================================================================
// export
//==============================================================================
module.exports = {
	CONFIG_MSG,
	COMPLETE_MSG,
	ERROR_MSG,
	TIMERON_MSG,
	TIMEROFF_MSG,
	FILETIPE_MSG,
	start,
	getHSCyclicMsec,
	getReScaningMsec,
	getHSFileType,
	setHSFileType,
	getHSManual,
	setHSManual,
	getTimerOn,
	setTimerOn,
	getTimerOff,
	setTimerOff,
	isNowTimerZone,
	checkTimerStart,
	checkTimerEnd
};

//==============================================================================
// 関数
//==============================================================================
function start() {
}

function getHSCyclicMsec() {
	return config.humanSensor.cyclicMsec;
}

// function setHSCyclicMsec(msec) {
// 	if (__isType('Number', msec) === true) {
// 		config.humanSensor.cyclicMsec = msec;
// 		__update(config);
// 	}
// 	else {
// 		console.log('[CFG]Not number.');
// 	}
// }

function getReScaningMsec() {
	return config.humanSensor.reScaningMsec;
}

// function setReScaningMsec(msec) {
// 	if (__isType('Number', msec) === true) {
// 		config.humanSensor.reScaningMsec = msec;
// 		__update(config);
// 	}
// 	else {
// 		console.log('[CFG]Not number.');
// 	}
// }

function getHSFileType() {
	return config.humanSensor.fileType;
}

function setHSFileType(fileType) {
	if (__isType('String', fileType) === true) {
		if ((fileType === 'IMAGE') || (fileType === 'VIDEO')) {
			config.humanSensor.fileType = fileType;
			__update(config);
			return true;
		}
		else {
			console.log('[CFG]Unknown string.');
			return false;
		}
	}
	else {
		console.log('[CFG]Not string.');
		return false;
	}
}

function getHSManual() {
	return config.humanSensor.manual;
}

function setHSManual(manual) {
	if (__isType('Boolean', manual) === true) {
		config.humanSensor.manual = manual;
		__update(config);
		return true;
	}
	else {
		console.log('[CFG]Not boolean.');
		return false;
	}
}

function getTimerOn() {
	if (config.humanSensor.timerStartHour === -1) {
		return 'OFF';
	}
	else {
		return config.humanSensor.timerStartHour.toString() + config.humanSensor.timerStartMin.toString();
	}
}

function setTimerOn(str) {
	if (__isType('String', str) === true) {
		if (str === 'OFF') {
			config.humanSensor.timerStartHour = -1;
			config.humanSensor.timerStartMin = -1;
			__update(config);
			return true;
		}

		if (str.length != 4) {
			return false;
		}

		var hour = Number(str.slice(0, 2));
		if (__isType('Number', hour) === false) {
			return false;
		}
		if ((hour < 0) || (23 < hour)) {
			return false;
		}

		var min = Number(str.slice(2, 4));
		if (__isType('Number', min) === false) {
			return false;
		}
		if ((min < 0) || (59 < min)) {
			return false;
		}

		config.humanSensor.timerStartHour = hour;
		config.humanSensor.timerStartMin = min;
		__update(config);
		return true;
	}
	else {
		console.log('[CFG]Not boolean.');
		return false;
	}
}

function getTimerOff() {
	return config.humanSensor.timerEndHour.toString() + config.humanSensor.timerEndMin.toString();
}

function setTimerOff(str) {
	if (__isType('String', str) === true) {
		if (str.length != 4) {
			return false;
		}

		var hour = Number(str.slice(0, 2));
		if (__isType('Number', hour) === false) {
			return false;
		}
		if ((hour < 0) || (23 < hour)) {
			return false;
		}

		var min = Number(str.slice(2, 4));
		if (__isType('Number', min) === false) {
			return false;
		}
		if ((min < 0) || (59 < min)) {
			return false;
		}

		config.humanSensor.timerEndHour = hour;
		config.humanSensor.timerEndMin = min;
		__update(config);
		return true;
	}
	else {
		console.log('[CFG]Not boolean.');
		return false;
	}
}

function isNowTimerZone() {
	// 開始時刻が-1ならタイマーOFF
	if (config.humanSensor.timerStartHour === -1) {
		return false;
	}

	var now = new Date();
	// 現在時刻を秒数にする
	var nowSec = (now.getHours() * 60 + now.getMinutes()) * 60 + now.getSeconds();
	// スタート時刻を秒数にする
	var startSec = (config.humanSensor.timerStartHour * 60 + config.humanSensor.timerStartMin) * 60;
	// 終了時刻を秒数にする
	var endSec = (config.humanSensor.timerEndHour * 60 + config.humanSensor.timerEndMin) * 60;

	// 判定
	if (startSec <= endSec) {
		return ((startSec <= nowSec) && (nowSec < endSec));
	}
	else {
		return ((nowSec < endSec) || (startSec <= nowSec));
	}
}

function checkTimerStart() {
	// 必ず1分周期で実行
	// 開始時刻が-1ならタイマーOFF
	if (config.humanSensor.timerStartHour === -1) {
		return false;
	}

	var now = new Date();
	var nowMin = now.getHours() * 60 + now.getMinutes();
	var startMin = config.humanSensor.timerStartHour * 60 + config.humanSensor.timerStartMin;

	if (nowMin === startMin) {
		return true;
	}
	else {
		return false;
	}
}

function checkTimerEnd() {
	// 必ず1分周期で実行
	// 開始時刻が-1ならタイマーOFF
	if (config.humanSensor.timerStartHour === -1) {
		return false;
	}

	var now = new Date();
	var nowMin = now.getHours() * 60 + now.getMinutes();
	var endMin = config.humanSensor.timerEndHour * 60 + config.humanSensor.timerEndMin;

	if (nowMin === endMin) {
		return true;
	}
	else {
		return false;
	}
}


function __update(config) {
	// 設定保存
	fs.writeFileSync('config.json', JSON.stringify(config, null, '    '));
}

function __isType(type, obj) {
	// 型チェック
	var clas = Object.prototype.toString.call(obj).slice(8, -1);
	return obj !== undefined && obj !== null && clas === type;
}
