//==============================================================================
// require
//==============================================================================
require('isomorphic-fetch');
require('date-utils');
const execSync = require('child_process').execSync;
var fs = require('fs');
var easyimg = require('easyimage');
var Dropbox = require('dropbox').Dropbox;
var async = require('async');

//==============================================================================
// export
//==============================================================================
module.exports = {
	execute: __capture
};

//==============================================================================
// 関数
//==============================================================================
function __capture(operation) {
	return new Promise(function (complete, failed) {
		// 戻り値
		var rtnObj = {
			capture_addr: 'https://',
			thumnail_addr: 'https://'
		};

		// Dropbox接続
		var dbx = new Dropbox({ accessToken: 'eepCubnK0jAAAAAAAAAABVIgZe-oSI4PL0edrmhEOJriWGFC2CPT0M-NC4pecDEY' });

		// 現在時刻からファイル名生成
		var date = new Date();
		var formattedDate = date.toFormat('YYYYMMDDHH24MISS');
		var fileName;
		var h264Name;
		var filePathLocal;
		var filePathDbox;
		var tmbName;
		var tmbPathLocal;
		var tmbPathDbox;

		if (operation === 'IMAGE') {
			// 静止画撮影
			fileName = formattedDate + '.jpg';
			filePathLocal = '/home/work/webcam/image/' + fileName;
			filePathDbox = '/pi/capture/image/' + fileName;
			tmbName = formattedDate + '_tmb.jpg';
			tmbPathLocal = '/home/work/webcam/image/' + tmbName;
			tmbPathDbox = '/pi/capture/image/' + tmbName;

			// 静止画キャプチャ実行
			execSync('raspistill -vf -hf -w 1440 -h 1080 -o ' + filePathLocal);
			// サムネイル作成
			execSync('ffmpeg -r 1 -i ' + filePathLocal + ' -f image2 -s 240x180 ' + tmbPathLocal);
			console.log('[CAP]Thumbnail created.');
		}
		else if (operation === 'VIDEO') {
			// 動画撮影
			h264Name = formattedDate + '.h264'
			var h264PathLocal = '/home/work/webcam/video/' + h264Name;
			fileName = formattedDate + '.mp4';
			filePathLocal = '/home/work/webcam/video/' + fileName;
			filePathDbox = '/pi/capture/video/' + fileName;
			tmbName = formattedDate + '_tmb.jpg';
			tmbPathLocal = '/home/work/webcam/video/' + tmbName;
			tmbPathDbox = '/pi/capture/video/' + tmbName;

			// 動画キャプチャ実行
			execSync('raspivid -vf -hf -o ' + h264PathLocal);
			// mp4へコンバート
			execSync('MP4Box -add ' + h264PathLocal + ' ' + filePathLocal);
			execSync('rm ' + h264PathLocal);
			// サムネイル作成
			execSync('ffmpeg -ss 0 -t 1 -r 1 -i ' + filePathLocal + ' -f image2 -s 240x135 ' + tmbPathLocal);
			console.log('[CAP]Thumbnail created.');
		}
		else {
			console.log('[CAP]Invalid operation.');
			failed('Invalid operation.');
		}

		// ファイル読み込み
		var capFd = fs.readFileSync(filePathLocal);
		var tmbFd = fs.readFileSync(tmbPathLocal);

		async.series([
			function (callback) {
				// キャプチャ画像・動画をdropboxにアップロード
				dbx.filesUpload({
					contents: capFd,
					path: filePathDbox
				})
					.then(function (response) {
						console.log('[CAP]Capture uploaded.');
						callback(null, 'upload capture');
					})
					.catch(function (error) {
						callback('[CAP]ERROR: ' + error.toString(), 'upload capture');
					});
			},
			function (callback) {
				// リンクを取得
				dbx.filesGetTemporaryLink({ path: filePathDbox })
					.then(function (response) {
						console.log('[CAP]Get capture link.');
						rtnObj.capture_addr = response.link;
						callback(null, 'get capture link');
					})
					.catch(function (error) {
						callback('[CAP]ERROR: ' + error.toString(), 'get capture link');
					});
			},
			function (callback) {
				// サムネイル画像をdropboxにアップロード
				dbx.filesUpload({
					contents: tmbFd,
					path: tmbPathDbox
				})
					.then(function (response) {
						console.log('[CAP]Tumbnail uploaded.');
						callback(null, 'upload thambnail');
					})
					.catch(function (error) {
						callback('[CAP]ERROR: ' + error.toString(), 'upload thambnail');
					});
			},
			function (callback) {
				// リンクを取得
				dbx.filesGetTemporaryLink({ path: tmbPathDbox })
					.then(function (response) {
						console.log('[CAP]Get tumbnail link.');
						rtnObj.thumnail_addr = response.link;
						callback(null, 'get thumbnail link');
					})
					.catch(function (error) {
						callback('[CAP]ERROR: ' + error.toString(), 'get thumbnail link');
					});
			}
		], function (error, result) {
			if (error) {
				console.log(error + ' @ ' + result);
				failed(error + ' @ ' + result);
			}
			console.log('[CAP]Series end.');
			complete(rtnObj);
		});
	});
}

