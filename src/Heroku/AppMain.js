//==============================================================================
// MQTT：require
//==============================================================================
var mqtt = require('mqtt');

//==============================================================================
// LINEBOT：require
//==============================================================================
var express = require('express');
var express = express();
var bodyParser = require('body-parser');
var request = require('request');
var crypto = require("crypto");

//==============================================================================
// 実装
//==============================================================================
// MQTT接続
var mqttClient = mqtt.connect(process.env.CLOUDMQTT_URL);
var timerId;
var webhookHandle;

// MQTT接続確立イベントハンドラ設定
mqttClient.on('connect', function () {
	console.log('[MQTT]Connected.');
	// サブスクライブ開始
	mqttClient.subscribe('homework-pi/from-pi/#');
})

// MQTT受信イベントハンドラ設定
mqttClient.on('message', function (topic, message) {
	clearTimeout(timerId);
	// messageはバッファなのでStringに変換
	console.log('[MQTT]Received: <' + topic + '> ' + message.toString());
	if (topic === 'homework-pi/from-pi/reply/text') {
		// LINEへテキストをリプライ
		replyTextToLINE(webhookHandle, message.toString());
	}
	else if (topic === 'homework-pi/from-pi/reply/image') {
		// LINEへ画像をリプライ
		var json = JSON.parse(message.toString());
		replyImageToLINE(webhookHandle, json.capture_addr, json.thumnail_addr);
	}
	else if (topic === 'homework-pi/from-pi/reply/video') {
		// LINEへ動画をリプライ
		var json = JSON.parse(message.toString());
		replyVideoToLINE(webhookHandle, json.capture_addr, json.thumnail_addr);
	}
	else if (topic === 'homework-pi/from-pi/push/text') {
		// LINEへテキストをPush
		pushTextToLINE(message.toString());
	}
	else if (topic === 'homework-pi/from-pi/push/image') {
		// LINEへ画像をpush
		var json = JSON.parse(message.toString());
		pushImageToLINE(json.capture_addr, json.thumnail_addr);
	}
	else if (topic === 'homework-pi/from-pi/push/video') {
		// LINEへ動画をリプライ
		var json = JSON.parse(message.toString());
		pushVideoToLINE(json.capture_addr, json.thumnail_addr);
	}
	else {
		console.log('[MQTT]Unknown topic.');
	}
})

// LINE webhookのlisten用ポート設定
express.set('port', (process.env.PORT || 8000));

// JSONの送信を許可
express.use(bodyParser.urlencoded({
	extended: true
}));

// JSONのパースを楽に（webhook受信時）
express.use(bodyParser.json());

// LINE webhook受信イベントハンドラ設定
express.post('/webhook', function (webhook, res) {
	// リクエストがLINE Platformから送られてきたか確認する
	if (!validate_signature(webhook.headers['x-line-signature'], webhook.body)) {
		console.log('[LINE]Invalid signature.');
		return;
	}
	// テキストが送られてきた場合のみ返事をする
	if ((webhook.body['events'][0]['type'] != 'message') || (webhook.body['events'][0]['message']['type'] != 'text')) {
		console.log('[LINE]Not text.');
		return;
	}
	console.log('[LINE]From : ' + webhook.body['events'][0]['source']['userId']);
	console.log('[LINE]Received : ' + webhook.body['events'][0]['message']['text']);
	webhookHandle = webhook;

	// LINEからのメッセージをMQTTにパブリッシュ
	mqttClient.publish('homework-pi/to-pi', webhook.body['events'][0]['message']['text']);
	console.log('[MQTT]Sended: ' + webhook.body['events'][0]['message']['text']);
	// 30秒でタイムアウト
	timerId = setTimeout(mqttTimeout, 30000);
});

// LINE webhookのlisten開始
express.listen(express.get('port'), function () {
	console.log('[LINE]App is running.');
});


//==============================================================================
// 関数
//==============================================================================
// 署名検証
function validate_signature(signature, body) {
	return signature == crypto.createHmac('sha256', process.env.LINE_CHANNEL_SECRET).update(new Buffer(JSON.stringify(body), 'utf8')).digest('base64');
}

// MQTTタイムアウトイベントハンドラ
function mqttTimeout() {
	console.log('[MQTT]Timeout.');
	// タイムアウトを通知
	var timeoutMsg = '[ERROR]\nリクエストがタイムアウトしました￼￼\u{100094}';
	replyTextToLINE(webhookHandle, timeoutMsg);
	// MQTT切断
	// mqttClient.end();
}

function replyTextToLINE(webhook, message) {
	// LINE送信データ作成
	var lineReplyData = {
		'replyToken': webhook.body['events'][0]['replyToken'],
		"messages": [{
			"type": "text",
			"text": message
		}]
	};
	replyToLINE(lineReplyData);
}

function replyImageToLINE(webhook, originalContentUrl, previewImageUrl) {
	// LINE送信データ作成
	var lineReplyData = {
		'replyToken': webhook.body['events'][0]['replyToken'],
		"messages": [{
			"type": "image",
			"originalContentUrl": originalContentUrl,
			"previewImageUrl": previewImageUrl
		}]
	};
	replyToLINE(lineReplyData);
}

function replyVideoToLINE(webhook, originalContentUrl, previewImageUrl) {
	// LINE送信データ作成
	var lineReplyData = {
		'replyToken': webhook.body['events'][0]['replyToken'],
		"messages": [{
			"type": "video",
			"originalContentUrl": originalContentUrl,
			"previewImageUrl": previewImageUrl
		}]
	};
	replyToLINE(lineReplyData);
}

function replyToLINE(lineReplyData) {
	// LINEヘッダーを定義
	var headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer {' + process.env.LINE_CHANNEL_ACCESS_TOKEN + '}',
	};
	// LINEオプションを定義
	var lineReplyOption = {
		url: 'https://api.line.me/v2/bot/message/reply',
		proxy: process.env.FIXIE_URL,
		headers: headers,
		json: true,
		body: lineReplyData
	};
	// LINEリプライ送信
	request.post(lineReplyOption, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log('[LINE]Reply sended: ' + JSON.stringify(body));
		}
		else {
			console.log('[LINE]Reply send error: ' + JSON.stringify(response));
		}
	});
}

function pushTextToLINE(message) {
	// LINE送信データ作成
	var lineReplyData = {
		'to': process.env.LINE_ID,
		"messages": [{
			"type": "text",
			"text": message
		}]
	};
	pushToLINE(lineReplyData);
}

function pushImageToLINE(originalContentUrl, previewImageUrl) {
	// LINE送信データ作成
	var lineReplyData = {
		'to': process.env.LINE_ID,
		"messages": [{
			"type": "image",
			"originalContentUrl": originalContentUrl,
			"previewImageUrl": previewImageUrl
		}]
	};
	pushToLINE(lineReplyData);
}

function pushVideoToLINE(originalContentUrl, previewImageUrl) {
	// LINE送信データ作成
	var lineReplyData = {
		'to': process.env.LINE_ID,
		"messages": [{
			"type": "video",
			"originalContentUrl": originalContentUrl,
			"previewImageUrl": previewImageUrl
		}]
	};
	pushToLINE(lineReplyData);
}

function pushToLINE(linePushData) {
	// LINEヘッダーを定義
	var headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer {' + process.env.LINE_CHANNEL_ACCESS_TOKEN + '}',
	};
	// LINEオプションを定義
	var lineReplyOption = {
		url: 'https://api.line.me/v2/bot/message/push',
		proxy: process.env.FIXIE_URL,
		headers: headers,
		json: true,
		body: linePushData
	};
	request.post(lineReplyOption, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log('[LINE]Push sended: ' + JSON.stringify(body));
		}
		else {
			console.log('[LINE]Push error: ' + JSON.stringify(response));
		}
	});
}
